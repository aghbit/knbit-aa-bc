KNBIT-AA
========

Authorization & Authentication for KNBIT system

Requirements
------------
- JDK 8
- MongoDB

Essential configuration
-----------------------

You need to specify your essential configuration: enabled profiles and address of database.
Default configuration is placed in `src/main/resources/application.properties.template`

Copy this file as `application.properties` and (optionally) change values to matching your settings.

Docker
------

### Building image from remote develop branch
`docker build -t <yourname>/knbit-aa:dev --no-cache -f docker/develop/Dockerfile .`

### Building image from current source
`docker build -t <yourname>/knbit-aa:dev -f docker/local/Dockerfile .`

### Running within container
`docker run -d --name <mongo-cointainer-name> mongo:3.0.3` - it will create container with running MongoDB 3.0.3

`docker run -d -P --link <mongo-container-name>:mongodb <yourname>/knbit-aa:dev` - it will create container with AA app linked to MongoDB

### Windows users
If you are running docker on Windows, remember to call `boot2docker ip` for correct IP resolution