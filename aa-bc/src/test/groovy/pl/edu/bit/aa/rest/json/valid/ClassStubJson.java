package pl.edu.bit.aa.rest.json.valid;

import lombok.Value;

@Value
public class ClassStubJson {
    private final String name;
    private final Integer age;

    private String getPrivate(){
        throw new AssertionError("private should not be called");
    }

    public String getWithArgs(int some_arg){
        throw new AssertionError("getters with args should not be called");
    }

    public String nonGetter(){
        throw new AssertionError("non-getter should not be called");
    }

}
