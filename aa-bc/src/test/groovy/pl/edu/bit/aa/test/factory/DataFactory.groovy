package pl.edu.bit.aa.test.factory

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pl.edu.bit.aa.db.PermissionRepository
import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.db.TokenRepository
import pl.edu.bit.aa.db.UserRepository
import pl.edu.bit.aa.entity.Permission
import pl.edu.bit.aa.entity.Role
import pl.edu.bit.aa.entity.Token
import pl.edu.bit.aa.entity.User

@Component
class DataFactory {

    private static final USER = [name: 'Joker', password: 'batmanhasnoparents', token: '1234']
    private static final ADMIN = [name: 'Batman', password: 'justice', token: '5678']

    @Autowired
    private UserRepository userRepository

    @Autowired
    private TokenRepository tokenRepository

    @Autowired
    private RolesRepository rolesRepository

    @Autowired
    private PermissionRepository permissionRepository

    private Role adminRole
    private adminPermission
    private userPermission

    private User user
    private User admin

    def flushDb() {
        permissionRepository.deleteAll()
        rolesRepository.deleteAll()
        tokenRepository.deleteAll()
        userRepository.deleteAll()
    }

    def prepareData() {
        preparePermissions()
        prepareRoles()
        prepareUsers()
        prepareTokens()
    }

    private def preparePermissions() {
        adminPermission = Permission.create("MANAGE_ROLES")
        userPermission = Permission.create("TEST")
        permissionRepository.save(adminPermission)
        permissionRepository.save(userPermission)
    }

    private def prepareRoles() {
        adminRole = Role.create("ADMIN", [Permission.create("MANAGE_ROLES")].toSet())
        rolesRepository.save(adminRole)
    }

    private def prepareUsers() {
        user = User.create(UUID.randomUUID(), USER.name, USER.password, true, [].toSet())
        admin = User.create(UUID.randomUUID(), ADMIN.name, ADMIN.password, true, [adminRole].toSet())

        userRepository.save(user)
        userRepository.save(admin)
    }

    private def prepareTokens() {
        Token userToken = Token.create(USER.token, user)
        Token adminToken = Token.create(ADMIN.token, admin)

        tokenRepository.save(userToken)
        tokenRepository.save(adminToken)
    }

    static def getUserToken() {
        return USER.token
    }

    static def getAdminToken() {
        return ADMIN.token
    }

    def injectRoleWithName(String name) {
        rolesRepository.save(Role.create(name.toUpperCase(), [Permission.create("TEST")].toSet()))
    }

    def flushTokens() {
        tokenRepository.deleteAll()
    }
}
