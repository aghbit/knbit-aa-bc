package pl.edu.bit.aa.rest.json.valid

import org.springframework.validation.Errors
import pl.edu.bit.aa.rest.exception.InvalidMessageBodyException
import spock.lang.Specification

/**
 * See {@link ClassStubJson} to see what functions will not be called during validation.
 */
class BasicValidatorTest extends Specification {

    Errors errors = Mock()
    ClassStubJson json;

    BasicValidator validator = new BasicValidator();

    def 'Validator should notice invalid string fields'() {
        given:
        json = new ClassStubJson(" ", 5)
        when:
        validator.validate(json, errors)
        then:
        thrown(InvalidMessageBodyException)
    }

    def 'Validator should notice non-string nulls'() {
        given:
        json = new ClassStubJson("janek", null)
        when:
        validator.validate(json, errors)
        then:
        thrown(InvalidMessageBodyException)
    }

    def 'Validator should allow valid request'() {
        given:
        json = new ClassStubJson("janek", 3)
        when:
        validator.validate(json, errors)
        then:
        notThrown(InvalidMessageBodyException)
    }
}
