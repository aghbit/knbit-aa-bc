package pl.edu.bit.aa.rest

import com.jayway.restassured.RestAssured
import com.jayway.restassured.http.ContentType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import pl.edu.bit.aa.Application
import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.entity.Permission
import pl.edu.bit.aa.rest.util.RequestConstant
import pl.edu.bit.aa.rest.util.exception.ExceptionToJsonConverter
import pl.edu.bit.aa.service.exception.AlreadyExistingRoleException
import pl.edu.bit.aa.service.exception.NoSuchPermissionException
import pl.edu.bit.aa.spring.annotation.WebIntegrationTest
import pl.edu.bit.aa.test.factory.DataFactory
import spock.lang.Specification

import javax.json.Json

import static com.jayway.restassured.RestAssured.given

/**
 * @author Wojciech Milewski
 */
@ContextConfiguration(loader = SpringApplicationContextLoader.class,
        classes = [Application.class])
@WebIntegrationTest
class UpdateControllerIT extends Specification {
    static final AUTH_HEADER = RequestConstant.AUTH_HEADER

    @Value('${local.server.port}')
    int portNumber

    @Autowired
    DataFactory factory

    @Autowired
    RolesRepository rolesRepository

    def setup() {
        RestAssured.port = portNumber
        factory.flushDb()
        factory.prepareData()
    }

    def cleanup() {
        factory.flushDb()
    }

    def 'RolesController should be able to change name of group correctly'() {
        given:
        factory.injectRoleWithName('ROLE')

        def requestBody = Json.createObjectBuilder()
                .add("name", "CHANGED_ROLE")
                .add("permissions", Json.createArrayBuilder().add(Permission.create("TEST").getName()))
                .build()
                .toString()

        def spec = given()
                .header(AUTH_HEADER, factory.getAdminToken())
                .contentType(ContentType.JSON)
                .body(requestBody)

        when:
        def response = spec.put("/roles/ROLE")
        then:
        HttpStatus.NO_CONTENT == HttpStatus.valueOf(response.statusCode())
        !rolesRepository.findRoleByName('ROLE').isPresent()
        def updatedRole = rolesRepository.findRoleByName('CHANGED_ROLE')
        updatedRole.isPresent()
        updatedRole.get().getPermissions() == [Permission.create("TEST")].toSet()
    }

    def 'RolesController should be able to change permissions correctly'() {
        given:
        factory.injectRoleWithName('role')

        def requestBody = Json.createObjectBuilder()
                .add("name", "ROLE")
                .add("permissions", Json.createArrayBuilder())
                .build()
                .toString()

        def spec = given()
                .header(AUTH_HEADER, factory.getAdminToken())
                .contentType(ContentType.JSON)
                .body(requestBody)

        when:
        def response = spec.put("/roles/ROLE")
        then:
        HttpStatus.NO_CONTENT == HttpStatus.valueOf(response.statusCode())
        def updatedRole = rolesRepository.findRoleByName('ROLE')
        updatedRole.isPresent()
        updatedRole.get().getPermissions() == [].toSet()
    }

    def 'RolesController should complain when role with updated name already exist'() {
        given:
        factory.injectRoleWithName('role')
        factory.injectRoleWithName('changedRole')

        def requestBody = Json.createObjectBuilder()
                .add("name", "changedRole")
                .add("permissions", Json.createArrayBuilder().add(Permission.create("TEST").getName()))
                .build()
                .toString()

        def spec = given()
                .header(AUTH_HEADER, factory.getAdminToken())
                .contentType(ContentType.JSON)
                .body(requestBody)

        def expectedBody = ExceptionToJsonConverter.toJson(new AlreadyExistingRoleException())

        when:
        def response = spec.put("/roles/role")
        then:
        HttpStatus.BAD_REQUEST == HttpStatus.valueOf(response.statusCode())
        expectedBody == response.getBody().asString()
    }

    def 'RolesController should complain when changed role is not found'() {
        given:
        def requestBody = Json.createObjectBuilder()
                .add("name", "changedRole")
                .add("permissions", Json.createArrayBuilder().add(Permission.create("TEST").getName()))
                .build()
                .toString()

        def spec = given()
                .header(AUTH_HEADER, factory.getAdminToken())
                .contentType(ContentType.JSON)
                .body(requestBody)

        when:
        def response = spec.put("/roles/role")
        then:
        HttpStatus.NOT_FOUND == HttpStatus.valueOf(response.statusCode())
    }

    def 'RolesController should complain when some permissions does not exist'() {
        given:
        factory.injectRoleWithName('role')

        def requestBody = Json.createObjectBuilder()
                .add("name", "changedRole")
                .add("permissions", Json.createArrayBuilder()
                .add(Permission.create("TEST").getName())
                .add("justkiddinname"))
                .build()
                .toString()

        def expectedBody = ExceptionToJsonConverter.toJson(new NoSuchPermissionException("justkiddinname"))

        def spec = given()
                .header(AUTH_HEADER, factory.getAdminToken())
                .contentType(ContentType.JSON)
                .body(requestBody)

        when:
        def response = spec.put("/roles/role")
        then:
        HttpStatus.BAD_REQUEST == HttpStatus.valueOf(response.statusCode())
        expectedBody == response.getBody().asString()
        !rolesRepository.findRoleByName('changedRole').isPresent()
    }

    def 'Role updated should fail when user is not logged in'() {
        given:
        factory.injectRoleWithName('role')
        factory.flushTokens()

        def requestBody = Json.createObjectBuilder()
                .add("name", "changedRole")
                .add("permissions", Json.createArrayBuilder().add(Permission.create("TEST").getName()))
                .build()
                .toString()

        def spec = given()
                .header(AUTH_HEADER, factory.getAdminToken())
                .contentType(ContentType.JSON)
                .body(requestBody)

        when:
        def response = spec.put("/roles/role")
        then:
        HttpStatus.UNAUTHORIZED == HttpStatus.valueOf(response.statusCode())
    }

    def 'Role updated should fail when user is not authorized'() {
        given:
        factory.injectRoleWithName('role')

        def requestBody = Json.createObjectBuilder()
                .add("name", "changedRole")
                .add("permissions", Json.createArrayBuilder().add(Permission.create("TEST").getName()))
                .build()
                .toString()

        def spec = given()
                .header(AUTH_HEADER, factory.getUserToken())
                .contentType(ContentType.JSON)
                .body(requestBody)

        when:
        def response = spec.put("/roles/role")
        then:
        HttpStatus.FORBIDDEN == HttpStatus.valueOf(response.statusCode())
    }
}
