package pl.edu.bit.aa.service

import pl.edu.bit.aa.db.PermissionRepository
import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.entity.Permission
import pl.edu.bit.aa.entity.Role
import pl.edu.bit.aa.service.exception.AlreadyExistingRoleException
import pl.edu.bit.aa.service.exception.NoSuchPermissionException
import spock.lang.Specification

class CreateRoleServiceTest extends Specification {

    final static ROLE_NAME = "BATMAN"
    static final NON_EXISTENT_PERMISSION = "permissionWhichDoesNotExist"
    static final TEST_PERMISSION_NAME = "TEST"
    static final TEST_PERMISSION =Permission.create(TEST_PERMISSION_NAME)
    final static PERMISSIONS = [TEST_PERMISSION].toSet()
    final static PERMISSIONS_NAMES = PERMISSIONS.collect { it.getName() }.toSet()

    RolesRepository rolesRepository = Mock()

    PermissionRepository permissionRepository = Mock()

    UserService userService = Mock()

    RolesService service = new RolesService(userService, rolesRepository, permissionRepository)

    def setup() {
        permissionRepository.findPermissionByName(TEST_PERMISSION_NAME) >> Optional.of(TEST_PERMISSION)
        permissionRepository.findPermissionByName(NON_EXISTENT_PERMISSION) >> Optional.empty()
    }

    def 'Role service should be able to create a role successfully'() {
        given:
        rolesRepository.findRoleByName(ROLE_NAME) >> Optional.empty()
        when:
        service.createRole(ROLE_NAME, PERMISSIONS_NAMES)
        then:
        1 * rolesRepository.save({ it.getName() == ROLE_NAME && it.getPermissions() == PERMISSIONS })
    }

    def 'Role service should throw an exception when role with such name already exists'() {
        given:
        rolesRepository.findRoleByName(ROLE_NAME) >> Optional.of(Mock(Role))
        when:
        service.createRole(ROLE_NAME, PERMISSIONS_NAMES)
        then:
        thrown(AlreadyExistingRoleException)
    }

    def 'Role service should throw an exception when permission does not exist'() {
        given:
        rolesRepository.findRoleByName(ROLE_NAME) >> Optional.empty()
        when:
        service.createRole(ROLE_NAME, [NON_EXISTENT_PERMISSION].toSet())
        then:
        def exception = thrown(NoSuchPermissionException)
        exception.getName() == NON_EXISTENT_PERMISSION
    }

}
