package pl.edu.bit.aa.rest.config;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import pl.edu.bit.aa.rest.util.exception.JsonException;

@RequiredArgsConstructor
@JsonException(HttpStatus.I_AM_A_TEAPOT)
public class TestException extends Exception {
    private final String name;
    private final String surname;
    private final int age;

    public String getName() {
        return name;
    }

    private String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public String getJob() {
        return "Batman";
    }
}