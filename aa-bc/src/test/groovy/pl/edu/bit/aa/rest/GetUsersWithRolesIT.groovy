package pl.edu.bit.aa.rest

import com.google.common.collect.Sets
import com.jayway.restassured.RestAssured
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import pl.edu.bit.aa.Application
import pl.edu.bit.aa.db.PermissionRepository
import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.db.TokenRepository
import pl.edu.bit.aa.db.UserRepository
import pl.edu.bit.aa.entity.Permission
import pl.edu.bit.aa.entity.Role
import pl.edu.bit.aa.entity.Token
import pl.edu.bit.aa.entity.User
import pl.edu.bit.aa.rest.util.RequestConstant
import pl.edu.bit.aa.spring.annotation.WebIntegrationTest
import spock.lang.Specification

import javax.json.Json

import static com.jayway.restassured.RestAssured.given

@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = [Application.class])
@WebIntegrationTest
class GetUsersWithRolesIT extends Specification {

    static final TOKEN = "1234"
    static final AUTH_HEADER = RequestConstant.AUTH_HEADER

    static final ADMIN_EMAIL = "admin@email.com"
    static final USER_1_EMAIL = "user1@email.com"
    static final USER_2_EMAIL = "user2@email.com"
    static final PASSWORD = "hiperPassword"
    static final UUID ADMIN_UUID = UUID.randomUUID()
    static final UUID USER_1_UUID = UUID.randomUUID()
    static final UUID USER_2_UUID = UUID.randomUUID()
    static final TEST_ROLE = "TEST_ROLE"

    static final ADMIN_ROLE = "ADMIN"

    static final REQUEST_PATH = "/users"

    Role adminRole
    Role userRole
    User admin
    Token token

    @Value('${local.server.port}')
    int portNumber

    @Autowired
    UserRepository userRepository

    @Autowired
    RolesRepository rolesRepository

    @Autowired
    TokenRepository tokenRepository

    @Autowired
    PermissionRepository permissionRepository

    void setup() {
        RestAssured.port = portNumber
        flushDb()
        preparePermissions()

        adminRole = Role.create(ADMIN_ROLE, Sets.newHashSet(Permission.create("MANAGE_ROLES"), Permission.create("MANAGE_USERS")))
        userRole = Role.create(TEST_ROLE, Collections.singleton(Permission.create("TEST")))
        rolesRepository.save(adminRole)
        rolesRepository.save(userRole)
        admin = User.create(ADMIN_UUID, ADMIN_EMAIL, PASSWORD, true, Collections.singleton(adminRole))
        User user1 = User.create(USER_1_UUID, USER_1_EMAIL, PASSWORD, true, Collections.singleton(userRole))
        User user2 = User.create(USER_2_UUID, USER_2_EMAIL, PASSWORD, false, Collections.emptySet())
        userRepository.save(admin)
        userRepository.save(user1)
        userRepository.save(user2)
        token = Token.create(TOKEN, admin)
        tokenRepository.save(token)

    }

    def flushDb() {
        permissionRepository.deleteAll()
        rolesRepository.deleteAll()
        tokenRepository.deleteAll()
        userRepository.deleteAll()
    }

    private def preparePermissions() {
        permissionRepository.save(Permission.create("MANAGE_ROLES"))
        permissionRepository.save(Permission.create("MANAGE_USERS"))
        permissionRepository.save(Permission.create("TEST"))
    }

    void cleanup() {
        flushDb()
    }

    def "Authorized query should list all members, their emails, roles and active status"() {
        given:
        def id_field = "id"
        def email_field = "email"
        def roles_field = "roles"
        def active_field = "active"
        def users_field = "users"
        def adminJson = Json.createObjectBuilder()
                .add(id_field, ADMIN_UUID.toString())
                .add(email_field, ADMIN_EMAIL)
                .add(roles_field, Json.createArrayBuilder()
                .add(ADMIN_ROLE))
                .add(active_field, true)
                .build()
        def user1Json = Json.createObjectBuilder()
                .add(id_field, USER_1_UUID.toString())
                .add(email_field, USER_1_EMAIL)
                .add(roles_field, Json.createArrayBuilder()
                .add(TEST_ROLE))
                .add(active_field, true)
                .build()
        def user2Json = Json.createObjectBuilder()
                .add(id_field, USER_2_UUID.toString())
                .add(email_field, USER_2_EMAIL)
                .add(roles_field, Json.createArrayBuilder())
                .add(active_field, false)
                .build()
        def expectedResponse = Json.createObjectBuilder()
                .add(users_field, Json.createArrayBuilder()
                .add(adminJson)
                .add(user1Json)
                .add(user2Json))
                .build().toString()
        def spec = given().header(AUTH_HEADER, TOKEN)
        when:
        def response = spec.get(REQUEST_PATH)
        then:
        response.getStatusCode() == HttpStatus.OK.value()
        response.asString() == expectedResponse
    }

    def "Query should fail if requester is not authenticated"() {
        given:
        tokenRepository.deleteAll()
        def spec = given().header(AUTH_HEADER, TOKEN)
        when:
        def response = spec.get(REQUEST_PATH)
        then:
        response.getStatusCode() == HttpStatus.UNAUTHORIZED.value()
    }

    def "Query should fail if requester is not authorized"() {
        given:
        userRepository.deleteAll()
        admin = User.create(ADMIN_UUID, ADMIN_EMAIL, PASSWORD, true, Collections.emptySet())
        userRepository.save(admin)
        def spec = given().header(AUTH_HEADER, TOKEN)
        when:
        def response = spec.get(REQUEST_PATH)
        then:
        response.getStatusCode() == HttpStatus.FORBIDDEN.value()
    }

}
