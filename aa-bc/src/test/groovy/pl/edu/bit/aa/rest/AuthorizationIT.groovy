package pl.edu.bit.aa.rest

import com.jayway.restassured.RestAssured
import com.jayway.restassured.http.ContentType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import pl.edu.bit.aa.Application
import pl.edu.bit.aa.db.PermissionRepository
import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.db.TokenRepository
import pl.edu.bit.aa.db.UserRepository
import pl.edu.bit.aa.entity.Permission
import pl.edu.bit.aa.entity.Role
import pl.edu.bit.aa.entity.Token
import pl.edu.bit.aa.entity.User
import pl.edu.bit.aa.rest.util.RequestConstant
import pl.edu.bit.aa.spring.annotation.WebIntegrationTest
import spock.lang.Specification

import javax.json.Json

import static com.jayway.restassured.RestAssured.given

@ContextConfiguration(loader = SpringApplicationContextLoader.class,
        classes = [Application.class])
@WebIntegrationTest
class AuthorizationIT extends Specification {
    private static final String AUTHORIZATION_PATH = '/authorize'

    private static final AUTH_TOKEN_HEADER = RequestConstant.AUTH_HEADER
    private static final PERMISSION_FIELD = "permission"

    private static final TOKEN = "1234"
    private static final PERMISSION_NAME = "TEST"
    private static final NOT_EXISTING_PERMISSION_NAME = "NOT_EXISTING_PERMISSION"

    @Value('${local.server.port}')
    int portNumber

    Permission adminPermission
    Permission userPermission

    @Autowired
    UserRepository userRepository

    @Autowired
    RolesRepository rolesRepository

    @Autowired
    TokenRepository tokenRepository

    @Autowired
    PermissionRepository permissionRepository


    void setup() {
        RestAssured.port = portNumber
        flushDb()
        preparePermissions()
    }

    void cleanup() {
        flushDb()
    }

    void flushDb() {
        permissionRepository.deleteAll()
        userRepository.deleteAll()
        rolesRepository.deleteAll()
        tokenRepository.deleteAll()
    }

    private def preparePermissions() {
        adminPermission = Permission.create("MANAGE_ROLES")
        userPermission = Permission.create("TEST")
        permissionRepository.save(adminPermission)
        permissionRepository.save(userPermission)
    }

    void fillDb() {
        Role role = Role.create("DARK_KNIGHT", [Permission.create("TEST")].toSet())
        fillDbWithRoles([role].toSet())
    }

    void fillDbWithRoles(Set<Role> roles) {
        User user = User.create(
                UUID.randomUUID(),
                "bruce@wayne.com",
                "iambatmanlol",
                true,
                roles
        )

        Token token = Token.create(TOKEN, user)

        roles.each {
            rolesRepository.save(it)
        }
        userRepository.save(user)
        tokenRepository.save(token)
    }

    def 'Authorization should be able to complete successfully'() {
        given:
        fillDb()
        def body = Json.createObjectBuilder()
                .add(PERMISSION_FIELD, PERMISSION_NAME)
                .build()
                .toString()

        def spec = given()
                .contentType(ContentType.JSON)
                .header(AUTH_TOKEN_HEADER, TOKEN)
                .body(body)
        when:
        def response = spec.post(AUTHORIZATION_PATH)
        then:
        HttpStatus.OK == HttpStatus.valueOf(response.getStatusCode())
    }

    def 'Authorization controller should return 401 when token is invalid'() {
        given:
        def body = Json.createObjectBuilder()
                .add(PERMISSION_FIELD, PERMISSION_NAME)
                .build()
                .toString()

        def spec = given()
                .contentType(ContentType.JSON)
                .header(AUTH_TOKEN_HEADER, "iaminvalid")
                .body(body)
        when:
        def response = spec.post(AUTHORIZATION_PATH)
        then:
        HttpStatus.UNAUTHORIZED == HttpStatus.valueOf(response.getStatusCode())
    }

    def 'Authorization controller should return 403 when user does not have desired permission'() {
        given:
        fillDbWithRoles([].toSet())

        def body = Json.createObjectBuilder()
                .add(PERMISSION_FIELD, PERMISSION_NAME)
                .build()
                .toString()

        def spec = given()
                .contentType(ContentType.JSON)
                .header(AUTH_TOKEN_HEADER, TOKEN)
                .body(body)
        when:
        def response = spec.post(AUTHORIZATION_PATH)
        then:
        HttpStatus.FORBIDDEN == HttpStatus.valueOf(response.getStatusCode())
    }

    def 'Authorization controller should return 403 when permission does not exist'() {
        given:
        fillDb()
        def body = Json.createObjectBuilder()
                .add(PERMISSION_FIELD, NOT_EXISTING_PERMISSION_NAME)
                .build()
                .toString()

        def spec = given()
                .contentType(ContentType.JSON)
                .header(AUTH_TOKEN_HEADER, TOKEN)
                .body(body)
        when:
        def response = spec.post(AUTHORIZATION_PATH)
        then:
        HttpStatus.FORBIDDEN == HttpStatus.valueOf(response.getStatusCode())
    }

}
