package pl.edu.bit.aa.rest

import com.jayway.restassured.RestAssured
import com.jayway.restassured.http.ContentType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import pl.edu.bit.aa.Application
import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.entity.Permission
import pl.edu.bit.aa.rest.util.RequestConstant
import pl.edu.bit.aa.rest.util.exception.ExceptionToJsonConverter
import pl.edu.bit.aa.service.exception.AlreadyExistingRoleException
import pl.edu.bit.aa.service.exception.NoSuchPermissionException
import pl.edu.bit.aa.spring.annotation.WebIntegrationTest
import pl.edu.bit.aa.test.factory.DataFactory
import spock.lang.Specification

import javax.json.Json

import static com.jayway.restassured.RestAssured.given

@ContextConfiguration(loader = SpringApplicationContextLoader.class,
        classes = [Application.class])
@WebIntegrationTest
class CreateRoleIT extends Specification {

    static final AUTH_HEADER = RequestConstant.AUTH_HEADER
    static final TEST_PERMISSION = "TEST"

    @Value('${local.server.port}')
    int portNumber

    @Autowired
    DataFactory factory

    @Autowired
    RolesRepository rolesRepository

    def setup() {
        RestAssured.port = portNumber
        factory.flushDb()
        factory.prepareData()
    }

    def cleanup() {
        factory.flushDb()
    }

    def 'RolesController should be able to correctly create a role'() {
        given:
        def requestBody = Json.createObjectBuilder()
                .add("name", "NEW_ROLE")
                .add("permissions", Json.createArrayBuilder().add(TEST_PERMISSION))
                .build()
                .toString()

        def spec = given()
                .contentType(ContentType.JSON)
                .header(AUTH_HEADER, factory.getAdminToken())
                .body(requestBody)
        when:
        def response = spec.post("/roles")
        then:
        HttpStatus.CREATED == HttpStatus.valueOf(response.statusCode())
        def savedRole = rolesRepository.findRoleByName("NEW_ROLE")
        savedRole.isPresent()
        savedRole.get().getName() == "NEW_ROLE"
        savedRole.get().getPermissions() == [Permission.create("TEST")].toSet()
    }

    def 'RolesController should return correct value and body when such role already exists'() {
        given:
        factory.injectRoleWithName("newGroup")

        def requestBody = Json.createObjectBuilder()
                .add("name", "newGroup")
                .add("permissions", Json.createArrayBuilder().add(TEST_PERMISSION))
                .build()
                .toString()

        def expectedBody = ExceptionToJsonConverter.toJson(new AlreadyExistingRoleException())

        def spec = given()
                .contentType(ContentType.JSON)
                .header(AUTH_HEADER, factory.getAdminToken())
                .body(requestBody)
        when:
        def response = spec.post("/roles")
        then:
        HttpStatus.BAD_REQUEST == HttpStatus.valueOf(response.statusCode())
        expectedBody == response.getBody().asString()
    }

    def 'RolesController should return correct value and body when there is no such permission'() {
        given:
        def requestBody = Json.createObjectBuilder()
                .add("name", "newGroup")
                .add("permissions", Json.createArrayBuilder().add("justkiddinname"))
                .build()
                .toString()

        def expectedBody = ExceptionToJsonConverter.toJson(new NoSuchPermissionException("justkiddinname"))

        def spec = given()
                .contentType(ContentType.JSON)
                .header(AUTH_HEADER, factory.getAdminToken())
                .body(requestBody)
        when:
        def response = spec.post("/roles")
        then:
        HttpStatus.BAD_REQUEST == HttpStatus.valueOf(response.statusCode())
        expectedBody == response.getBody().asString()
    }

    def 'Role creation should fail when user is not logged in'() {
        given:
        factory.flushTokens()

        def requestBody = Json.createObjectBuilder()
                .add("name", "newGroup")
                .add("permissions", Json.createArrayBuilder().add(TEST_PERMISSION))
                .build()
                .toString()

        def spec = given()
                .contentType(ContentType.JSON)
                .header(AUTH_HEADER, factory.getAdminToken())
                .body(requestBody)
        when:
        def response = spec.post("/roles")
        then:
        HttpStatus.UNAUTHORIZED == HttpStatus.valueOf(response.statusCode())
    }

    def 'Role creation should fail when user is not authorized'() {
        given:
        def requestBody = Json.createObjectBuilder()
                .add("name", "newGroup")
                .add("permissions", Json.createArrayBuilder().add(TEST_PERMISSION))
                .build()
                .toString()

        def spec = given()
                .contentType(ContentType.JSON)
                .header(AUTH_HEADER, factory.getUserToken())
                .body(requestBody)
        when:
        def response = spec.post("/roles")
        then:
        HttpStatus.FORBIDDEN == HttpStatus.valueOf(response.statusCode())
    }
}
