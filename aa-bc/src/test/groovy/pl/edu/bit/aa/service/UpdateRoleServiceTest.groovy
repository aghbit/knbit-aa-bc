package pl.edu.bit.aa.service

import pl.edu.bit.aa.db.PermissionRepository
import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.entity.Permission
import pl.edu.bit.aa.entity.Role
import pl.edu.bit.aa.service.exception.AlreadyExistingRoleException
import pl.edu.bit.aa.service.exception.NoSuchPermissionException
import pl.edu.bit.aa.service.exception.RoleNotFoundException
import spock.lang.Specification

class UpdateRoleServiceTest extends Specification {

    static final OLD_NAME = 'BATMAN'
    static final NEW_NAME = 'THE_DARK_KNIGHT'
    static final NON_EXISTENT_PERMISSION = "permissionWhichDoesNotExist"
    static final TEST_PERMISSION_NAME = "TEST"
    static final TEST_PERMISSION =Permission.create(TEST_PERMISSION_NAME)
    static final OLD_PERMISSIONS = [TEST_PERMISSION].toSet()
    static final OLD_PERMISSIONS_NAMES = OLD_PERMISSIONS.collect { it.getName()}.toSet()
    static final NEW_PERMISSIONS_NAMES = [].toSet()

    RolesRepository rolesRepository = Mock()

    PermissionRepository permissionRepository = Mock()

    UserService userService = Mock()

    def setup() {
        permissionRepository.findPermissionByName(TEST_PERMISSION_NAME) >> Optional.of(TEST_PERMISSION)
        permissionRepository.findPermissionByName(NON_EXISTENT_PERMISSION) >> Optional.empty()
    }

    RolesService service = new RolesService(userService, rolesRepository, permissionRepository)

    def 'Service should be able to change name of role successfully'() {
        given:
        Role role = Mock(Role)
        rolesRepository.findRoleByName(OLD_NAME) >> Optional.of(role)
        rolesRepository.findRoleByName(NEW_NAME) >> Optional.empty()
        when:
        service.update(OLD_NAME, NEW_NAME, OLD_PERMISSIONS_NAMES)
        then:
        1 * rolesRepository.delete(role)
        1 * rolesRepository.save({ it.getName() == NEW_NAME && it.getPermissions() == OLD_PERMISSIONS })
    }

    def 'Service should be able to change permissions of role successfully'() {
        given:
        Role role = Mock(Role)
        rolesRepository.findRoleByName(OLD_NAME) >> Optional.of(role)
        rolesRepository.findRoleByName(NEW_NAME) >> Optional.empty()
        when:
        service.update(OLD_NAME, OLD_NAME, NEW_PERMISSIONS_NAMES)
        then:
        0 * rolesRepository.delete(role)
        1 * rolesRepository.save({ it.getName() == OLD_NAME && it.getPermissions() == NEW_PERMISSIONS_NAMES })
    }

    def 'Service should prevent changing role name to the existing one by throwing an exception'() {
        given:
        Role role = Mock(Role)
        rolesRepository.findRoleByName(OLD_NAME) >> Optional.of(role)
        rolesRepository.findRoleByName(NEW_NAME) >> Optional.of(Mock(Role))
        when:
        service.update(OLD_NAME, NEW_NAME, NEW_PERMISSIONS_NAMES)
        then:
        thrown(AlreadyExistingRoleException)
    }

    def 'Service should prevent changing role when it does not exist'() {
        given:
        rolesRepository.findRoleByName(OLD_NAME) >> Optional.empty()
        when:
        service.update(OLD_NAME, NEW_NAME, NEW_PERMISSIONS_NAMES)
        then:
        thrown(RoleNotFoundException)
    }

    def 'Service should throw an exception when permission does not exist'() {
        given:
        Role role = Mock(Role)
        rolesRepository.findRoleByName(OLD_NAME) >> Optional.of(role)
        rolesRepository.findRoleByName(NEW_NAME) >> Optional.empty()
        when:
        service.update(OLD_NAME, NEW_NAME, [NON_EXISTENT_PERMISSION].toSet())
        then:
        def exception = thrown(NoSuchPermissionException)
        exception.getName() == NON_EXISTENT_PERMISSION
    }
}
