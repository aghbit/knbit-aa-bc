package pl.edu.bit.aa.rest.config

import org.springframework.http.HttpStatus
import spock.lang.Specification

import javax.json.Json

class JsonExceptionHandlerTest extends Specification {
    public static final String NAME = "Bruce"
    public static final String SURNAME = "Wayne"
    public static final int AGE = 30
    public static final String JOB = "Batman"

    def testedInstance = new JsonExceptionHandler()

    def exception = new TestException(NAME, SURNAME, AGE)

    def "JsonExceptionHandler should return response with proper HttpStatus code"() {
        when:
        def result = testedInstance.handle(this.exception)

        then:
        result.getStatusCode() == HttpStatus.I_AM_A_TEAPOT
    }

    def "JsonExceptionHandler should set up correct body"() {
        given:
        def jsonBuilder = Json.createObjectBuilder()
        jsonBuilder.add("name", NAME)
        jsonBuilder.add("age", AGE)
        jsonBuilder.add("job", JOB)
        def expectedJson = jsonBuilder.build()

        when:
        def result = testedInstance.handle(exception)

        then:
        Json.createReader(new StringReader(result.getBody())).readObject() == expectedJson
    }
}