package pl.edu.bit.aa.service

import pl.edu.bit.aa.db.PermissionRepository
import pl.edu.bit.aa.db.UserRepository
import pl.edu.bit.aa.entity.Permission
import pl.edu.bit.aa.entity.Role
import pl.edu.bit.aa.entity.User
import spock.lang.Specification

class AuthorizationServiceTest extends Specification {
    private static final String PERMISSION_NAME = "TEST"
    static final TEST_PERMISSION =Permission.create(PERMISSION_NAME)
    private static final String NON_EXISTENT_PERMISSION = "permissionWhichDoesNotExist"

    UserRepository userRepository = Mock()

    PermissionRepository permissionRepository = Mock()

    private Role firstRole = Mock()
    private Role secondRole = Mock()
    private String userId = UUID.randomUUID().toString()
    private User user = Mock()

    AuthorizationService service = new AuthorizationService(userRepository, permissionRepository)

    def setup() {
        permissionRepository.findPermissionByName(PERMISSION_NAME) >> Optional.of(TEST_PERMISSION)
        permissionRepository.findPermissionByName(NON_EXISTENT_PERMISSION) >> Optional.empty()
    }

    def "AuthorizationService should correctly verify if user has correct permission"() {
        given:
        this.firstRole.getPermissions() >> []
        this.secondRole.getPermissions() >> [Permission.create(PERMISSION_NAME)]

        this.user.getRoles() >> [this.firstRole, this.secondRole]

        userRepository.findById(_) >> Optional.of(this.user)

        when:
        def userHasPermission = service.userHasPermission(userId, PERMISSION_NAME)
        then:
        userHasPermission
    }

    def "AuthorizationService should return correct value when such permission does not exist"() {
        given:
        firstRole.getPermissions() >> [Permission.create("TEST")]

        user.getRoles() >> [firstRole]

        userRepository.findById(_) >> Optional.of(user)

        when:
        def userHasPermission = service.userHasPermission(userId, NON_EXISTENT_PERMISSION)
        then:
        !userHasPermission
    }

    def 'AuthorizationService should return correct value when user does not have such permission'() {
        given:
        firstRole.getPermissions() >> []
        secondRole.getPermissions() >> []

        user.getRoles() >> [firstRole, secondRole]

        userRepository.findById(_) >> Optional.of(user)

        when:
        def userHasPermission = service.userHasPermission(this.userId, PERMISSION_NAME)
        then:
        !userHasPermission
    }

    def 'AuthorizationService should return correct value when user does not exist'() {
        given:
        userRepository.findById(_) >> Optional.empty()
        when:
        def userHasPermission = service.userHasPermission(userId, PERMISSION_NAME)
        then:
        !userHasPermission
    }
}
