package pl.edu.bit.aa.rest

import com.jayway.restassured.RestAssured
import com.jayway.restassured.http.ContentType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import pl.edu.bit.aa.Application
import pl.edu.bit.aa.db.PermissionRepository
import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.db.TokenRepository
import pl.edu.bit.aa.db.UserRepository
import pl.edu.bit.aa.entity.Permission
import pl.edu.bit.aa.entity.Role
import pl.edu.bit.aa.entity.Token
import pl.edu.bit.aa.entity.User
import pl.edu.bit.aa.rest.exception.InvalidMessageBodyException
import pl.edu.bit.aa.rest.util.RequestConstant
import pl.edu.bit.aa.rest.util.exception.ExceptionToJsonConverter
import pl.edu.bit.aa.spring.annotation.WebIntegrationTest
import spock.lang.Specification

import javax.json.Json

import static com.jayway.restassured.RestAssured.given

@ContextConfiguration(loader = SpringApplicationContextLoader.class,
        classes = [Application.class])
@WebIntegrationTest
public class RoleAssignmentIT extends Specification {

    static final TOKEN = "1234"

    static final ROLE_NAME_FIELD = "roleName"
    static final AUTH_HEADER = RequestConstant.AUTH_HEADER

    static final ADMIN_EMAIL = "admin@email.com"
    static final USER_EMAIL = "user@email.com"
    private static final PASSWORD = "hiperPassword"
    static final UUID ADMIN_UUID = UUID.randomUUID()
    static final UUID USER_UUID = UUID.randomUUID()
    static final USER_ID = USER_UUID.toString()
    static final TEST_ROLE = "TEST_ROLE"

    static final ADMIN_ROLE = "ADMIN"

    static final ROLE_ASSIGNMENT_PATH = "/users/" + USER_ID + "/roles"

    Role adminRole
    Role roleToBeAssigned
    User admin
    User user
    Token token
    Permission adminPermission
    Permission userPermission

    @Value('${local.server.port}')
    int portNumber

    @Autowired
    RolesRepository rolesRepository

    @Autowired
    UserRepository userRepository

    @Autowired
    TokenRepository tokenRepository

    @Autowired
    PermissionRepository permissionRepository

    void setup() {
        RestAssured.port = portNumber
        flushDb()
        preparePermissions()

        adminRole = Role.create(ADMIN_ROLE, Collections.singleton(Permission.create("MANAGE_ROLES")))
        roleToBeAssigned = Role.create(TEST_ROLE, Collections.singleton(Permission.create("TEST")))
        rolesRepository.save(adminRole)
        rolesRepository.save(roleToBeAssigned)
        admin = User.create(ADMIN_UUID, ADMIN_EMAIL, PASSWORD, true, Collections.singleton(adminRole))
        user = User.create(USER_UUID, USER_EMAIL, PASSWORD, true, Collections.emptySet()) // user has no roles
        userRepository.save(admin)
        userRepository.save(user)
        token = Token.create(TOKEN, admin)
        tokenRepository.save(token)
    }

    def flushDb() {
        permissionRepository.deleteAll()
        rolesRepository.deleteAll()
        tokenRepository.deleteAll()
        userRepository.deleteAll()
    }

    private def preparePermissions() {
        adminPermission = Permission.create("MANAGE_ROLES")
        userPermission = Permission.create("TEST")
        permissionRepository.save(adminPermission)
        permissionRepository.save(userPermission)
    }

    void cleanup() {
        flushDb()
    }

    def "Admin should be able to assign role to user"() {
        given:
        def requestBody = Json.createObjectBuilder().add(ROLE_NAME_FIELD, TEST_ROLE)
                .build().toString()
        def spec = given().contentType(ContentType.JSON).body(requestBody).header(AUTH_HEADER, TOKEN)
        when:
        def response = spec.put(ROLE_ASSIGNMENT_PATH)
        then:
        response.getStatusCode() == HttpStatus.NO_CONTENT.value()
        userRepository.findById(USER_ID).get().getRoles().contains(roleToBeAssigned)
    }

    def "Role assignment request with missing Role should result in appropriate status and message"() {
        given:
        def requestBody = Json.createObjectBuilder()
                .build().toString()
        def spec = given().contentType(ContentType.JSON).body(requestBody).header(AUTH_HEADER, TOKEN)
        def expectedResponse = ExceptionToJsonConverter.toJson(new InvalidMessageBodyException())
        when:
        def response = spec.put(ROLE_ASSIGNMENT_PATH)
        then:
        response.getStatusCode() == HttpStatus.BAD_REQUEST.value()
        response.asString() == expectedResponse
    }

    def "Role assignment should fail if assigner is not authenticated"() {
        given:
        tokenRepository.deleteAll()
        def requestBody = Json.createObjectBuilder().add(ROLE_NAME_FIELD, TEST_ROLE)
                .build().toString()
        def spec = given().contentType(ContentType.JSON).body(requestBody).header(AUTH_HEADER, TOKEN)
        when:
        def response = spec.put(ROLE_ASSIGNMENT_PATH)
        then:
        response.getStatusCode() == HttpStatus.UNAUTHORIZED.value()
    }

    def "Role assignment should fail if assigner is not authorized"() {
        given:
        userRepository.deleteAll()
        admin = User.create(ADMIN_UUID, ADMIN_EMAIL, PASSWORD, true, Collections.emptySet())
        userRepository.save(admin)
        def requestBody = Json.createObjectBuilder().add(ROLE_NAME_FIELD, TEST_ROLE)
                .build().toString()
        def spec = given().contentType(ContentType.JSON).body(requestBody).header(AUTH_HEADER, TOKEN)
        when:
        def response = spec.put(ROLE_ASSIGNMENT_PATH)
        then:
        response.getStatusCode() == HttpStatus.FORBIDDEN.value()
    }

    def "Role assignment to non-existent user should cause appropriate status and message"() {
        given:
        userRepository.deleteAll()
        userRepository.save(admin)
        def requestBody = Json.createObjectBuilder().add(ROLE_NAME_FIELD, TEST_ROLE)
                .build().toString()
        def spec = given().contentType(ContentType.JSON).body(requestBody).header(AUTH_HEADER, TOKEN)
        when:
        def response = spec.put("/users/" + UUID.randomUUID().toString() + "/roles")
        then:
        response.getStatusCode() == HttpStatus.BAD_REQUEST.value()
    }

    def "Role assignment of non-existent role should cause appropriate status and message"() {
        given:
        rolesRepository.deleteAll()
        rolesRepository.save(adminRole)
        def requestBody = Json.createObjectBuilder().add(ROLE_NAME_FIELD, TEST_ROLE)
                .build().toString()
        def spec = given().contentType(ContentType.JSON).body(requestBody).header(AUTH_HEADER, TOKEN)
        when:
        def response = spec.put(ROLE_ASSIGNMENT_PATH)
        then:
        response.getStatusCode() == HttpStatus.BAD_REQUEST.value()
    }
}
