package pl.edu.bit.aa.rest

import com.jayway.restassured.RestAssured
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import pl.edu.bit.aa.Application
import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.rest.util.RequestConstant
import pl.edu.bit.aa.spring.annotation.WebIntegrationTest
import pl.edu.bit.aa.test.factory.DataFactory
import spock.lang.Specification

import javax.json.Json

import static com.jayway.restassured.RestAssured.given

/**
 * @author Wojciech Milewski
 */
@ContextConfiguration(loader = SpringApplicationContextLoader.class,
        classes = [Application.class])
@WebIntegrationTest
class ReadRoleIT extends Specification {

    static final AUTH_HEADER = RequestConstant.AUTH_HEADER

    @Value('${local.server.port}')
    int portNumber

    @Autowired
    DataFactory factory

    @Autowired
    RolesRepository rolesRepository

    def setup() {
        RestAssured.port = portNumber
        factory.flushDb()
        factory.prepareData()
    }

    def cleanup() {
        factory.flushDb()
    }

    def 'RolesController should be able to correctly return existing role'() {
        given:
        factory.injectRoleWithName('ROLE')

        def expectedBody = Json.createObjectBuilder()
                .add("name", "ROLE")
                .add("permissions", Json.createArrayBuilder().add("TEST"))
                .build()
                .toString()

        def spec = given()
                .header(AUTH_HEADER, factory.getAdminToken())
        when:
        def response = spec.get("/roles/ROLE")

        then:
        HttpStatus.OK == HttpStatus.valueOf(response.statusCode())
        expectedBody == response.body().asString()
    }

    def 'RolesController should return 404 when such role does not exist'() {
        given:
        def spec = given()
                .header(AUTH_HEADER, factory.getAdminToken())
        when:
        def response = spec.get("/roles/role")

        then:
        HttpStatus.NOT_FOUND == HttpStatus.valueOf(response.statusCode())
    }

    def 'Role showing should fail when user is not logged in'() {
        given:
        factory.flushTokens()
        factory.injectRoleWithName("role")

        def spec = given()
                .header(AUTH_HEADER, factory.getAdminToken())
        when:
        def response = spec.get("/roles/role")
        then:
        HttpStatus.UNAUTHORIZED == HttpStatus.valueOf(response.statusCode())
    }

    def 'Role showing should fail when user is not authorized'() {
        given:
        factory.injectRoleWithName("role")

        def spec = given()
                .header(AUTH_HEADER, factory.getUserToken())
        when:
        def response = spec.get("/roles/role")
        then:
        HttpStatus.FORBIDDEN == HttpStatus.valueOf(response.statusCode())
    }
}
