package pl.edu.bit.aa.rest.json.valid

import com.jayway.restassured.RestAssured
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import pl.edu.bit.aa.Application
import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.rest.util.RequestConstant
import pl.edu.bit.aa.spring.annotation.WebIntegrationTest
import pl.edu.bit.aa.test.factory.DataFactory
import spock.lang.Specification

import static com.jayway.restassured.RestAssured.given

/**
* @author Wojciech Milewski
*/

@ContextConfiguration(loader = SpringApplicationContextLoader.class,
        classes = [Application.class])
@WebIntegrationTest
class DeleteRoleIT extends Specification {

    static final AUTH_HEADER = RequestConstant.AUTH_HEADER

    @Value('${local.server.port}')
    int portNumber

    @Autowired
    DataFactory factory

    @Autowired
    RolesRepository rolesRepository

    def setup() {
        RestAssured.port = portNumber
        factory.flushDb()
        factory.prepareData()
    }

    def cleanup() {
        factory.flushDb()
    }

    def 'RolesController should be able to correctly delete a role'() {
        given:
        factory.injectRoleWithName("deletedGroup")

        def spec = given()
                .header(AUTH_HEADER, factory.getAdminToken())
        when:
        def response = spec.delete("/roles/deletedGroup")
        then:
        HttpStatus.NO_CONTENT == HttpStatus.valueOf(response.statusCode())
        def savedRole = rolesRepository.findRoleByName("deletedGroup")
        !savedRole.isPresent()
    }

    def 'RolesController should behave the same way when there is no role to delete'() {
        given:
        def spec = given()
                .header(AUTH_HEADER, factory.getAdminToken())
        when:
        def response = spec.delete("/roles/deletedGroup")
        then:
        HttpStatus.NO_CONTENT == HttpStatus.valueOf(response.statusCode())
        def savedRole = rolesRepository.findRoleByName("deletedGroup")
        !savedRole.isPresent()
    }

    def 'Role creation should fail when user is not logged in'() {
        given:
        factory.flushTokens()
        factory.injectRoleWithName("deletedGroup")

        def spec = given()
                .header(AUTH_HEADER, factory.getAdminToken())
        when:
        def response = spec.delete("/roles/deletedGroup")
        then:
        HttpStatus.UNAUTHORIZED == HttpStatus.valueOf(response.statusCode())
    }

    def 'Role creation should fail when user is not authorized'() {
        given:
        factory.injectRoleWithName("deletedGroup")

        def spec = given()
                .header(AUTH_HEADER, factory.getUserToken())
        when:
        def response = spec.delete("/roles/deletedGroup")
        then:
        HttpStatus.FORBIDDEN == HttpStatus.valueOf(response.statusCode())
    }
}
