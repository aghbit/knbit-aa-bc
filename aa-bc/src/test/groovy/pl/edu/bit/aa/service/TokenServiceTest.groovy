package pl.edu.bit.aa.service

import org.jasypt.digest.StringDigester
import org.jasypt.util.password.PasswordEncryptor
import pl.edu.bit.aa.db.TokenRepository
import pl.edu.bit.aa.db.UserRepository
import pl.edu.bit.aa.entity.Token
import pl.edu.bit.aa.entity.User
import pl.edu.bit.aa.service.exception.InvalidCredentialsException
import spock.lang.Specification

class TokenServiceTest extends Specification {

    static final String stringToken = "12345"
    static final String userId = UUID.randomUUID().toString()
    static final String email = "mejlinator4TheWin"
    static final String password = "alamakota"

    TokenRepository tokenRepository = Mock()
    UserRepository userRepository = Mock()
    PasswordEncryptor encryptor = Mock()
    StringDigester digester = Mock()

    TokenService tokenService = new TokenService(tokenRepository, userRepository, encryptor, digester)

    def 'TokenService should be able to confirm valid token'() {
        given:
        Token token = Mock()
        tokenRepository.findTokenAndUpdate(stringToken) >> Optional.of(token)
        when:
        def tokenIsValid = tokenService.isTokenValid(stringToken)
        then:
        tokenIsValid
    }

    def 'TokenService should be able to confirm invalid token'() {
        given:
        tokenRepository.findTokenAndUpdate(stringToken) >> Optional.empty()
        when:
        def tokenIsValid = tokenService.isTokenValid(stringToken)
        then:
        !tokenIsValid
    }

    def 'TokenService should be able to provide new token by userId and password'() {
        given:
        User user = Mock()
        def encryptedPassword = password + "encrypted"
        def newTokenString = "tokenSoProvided"
        user.getPassword() >> encryptedPassword
        user.isActive() >> true
        digester.digest(_ as String) >> newTokenString
        encryptor.checkPassword(password, encryptedPassword) >> true
        userRepository.findById(userId) >> Optional.of(user)
        tokenRepository.findTokenAndUpdate(newTokenString) >> Optional.empty();
        when:
        def providedToken = tokenService.provideTokenByUserId(userId, password)
        then:
        1 * tokenRepository.save(_ as Token);
        providedToken.getToken().equals(newTokenString)
    }

    def 'TokenService should be able to provide new token by mail and password'() {
        given:
        User user = Mock()
        def encryptedPassword = password + "encrypted"
        def newTokenString = "tokenSoProvided"
        user.getPassword() >> encryptedPassword
        user.isActive() >> true
        digester.digest(_ as String) >> newTokenString
        encryptor.checkPassword(password, encryptedPassword) >> true
        userRepository.findByEmail(email) >> Optional.of(user)
        tokenRepository.findTokenAndUpdate(newTokenString) >> Optional.empty();
        when:
        def providedToken = tokenService.provideTokenByEmail(email, password)
        then:
        1 * tokenRepository.save(_ as Token);
        providedToken.getToken().equals(newTokenString)
    }

    def 'TokenService should not accept token request with missing userId'() {
        given:
        userRepository.findById(userId) >> Optional.empty()
        when:
        tokenService.provideTokenByUserId(userId, password)
        then:
        thrown(InvalidCredentialsException)
    }

    def 'TokenService should not accept token request with missing email'() {
        given:
        userRepository.findByEmail(email) >> Optional.empty()
        when:
        tokenService.provideTokenByEmail(email, password)
        then:
        thrown(InvalidCredentialsException)
    }

    def 'TokenService should not accept token request with correct userId and invalid credentials'() {
        given:
        User user = Mock()
        def invalidPassword = password + "invalid"
        user.getPassword() >> invalidPassword
        user.isActive() >> true
        userRepository.findById(userId) >> Optional.of(user)
        encryptor.checkPassword(password, invalidPassword) >> false
        when:
        tokenService.provideTokenByUserId(userId, password)
        then:
        thrown(InvalidCredentialsException)
    }

    def 'TokenService should not accept token request with correct email and invalid credentials'() {
        given:
        User user = Mock()
        def invalidPassword = password + "invalid"
        user.getPassword() >> invalidPassword
        user.isActive() >> true
        userRepository.findByEmail(email) >> Optional.of(user)
        encryptor.checkPassword(password, invalidPassword) >> false
        when:
        tokenService.provideTokenByEmail(email, password)
        then:
        thrown(InvalidCredentialsException)
    }

    def 'TokenService should perform token deletion in an idempotent manner'(){
        given:
        String stringToken = "someToken"
        when:
        tokenService.removeToken(stringToken)
        tokenService.removeToken(stringToken)
        tokenService.removeToken(stringToken)
        then:
        3 * tokenRepository.delete({it.equals(stringToken)})
    }

}
