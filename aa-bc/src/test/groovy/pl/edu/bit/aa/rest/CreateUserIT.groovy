package pl.edu.bit.aa.rest

import com.jayway.restassured.RestAssured
import com.jayway.restassured.http.ContentType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import pl.edu.bit.aa.Application
import pl.edu.bit.aa.db.UserRepository
import pl.edu.bit.aa.entity.User
import pl.edu.bit.aa.rest.exception.InvalidMessageBodyException
import pl.edu.bit.aa.rest.util.exception.ExceptionToJsonConverter
import pl.edu.bit.aa.service.exception.AlreadyExistingUserException
import pl.edu.bit.aa.service.exception.InvalidEmailException
import pl.edu.bit.aa.service.exception.PasswordTooWeakException
import pl.edu.bit.aa.spring.annotation.WebIntegrationTest
import spock.lang.Specification

import javax.json.Json

import static com.jayway.restassured.RestAssured.given

@ContextConfiguration(loader = SpringApplicationContextLoader.class,
classes = [Application.class])
@WebIntegrationTest
class CreateUserIT extends Specification {

    static final EMAIL = "bruce@wayne.com"
    static final PASSWORD = "ihavenoparents"

    @Value('${local.server.port}')
    int portNumber

    @Autowired
    UserRepository userRepository

    String bodyJson

    void setup() {
        RestAssured.port = portNumber
        userRepository.deleteAll()
        bodyJson = prepareBodyJson()
    }

    void cleanup() {
        userRepository.deleteAll()
    }

    private static prepareBodyJson() {
        def jsonBuilder = Json.createObjectBuilder()
        jsonBuilder.add("email", EMAIL)
        jsonBuilder.add("password", PASSWORD)
        return jsonBuilder.build().toString()
    }

    def "UserController should return proper message after successful user creation"() {
        given:
        def spec = given().contentType(ContentType.JSON).body(bodyJson)
        when:
        def response = spec.post("/users")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.CREATED
    }

    def "UserController should return proper code and body when user exists"() {
        given:
        userRepository.save(User.create(EMAIL, PASSWORD))

        def expectedResponseJson = ExceptionToJsonConverter.toJson(new AlreadyExistingUserException(EMAIL))

        def spec = given().contentType(ContentType.JSON).body(bodyJson)
        when:
        def response = spec.post("/users")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.BAD_REQUEST
        response.asString() == expectedResponseJson
    }

    def "UserController should return proper code and information when email is missing in the request"() {
        given:
        def expectedResponseJson = ExceptionToJsonConverter.toJson(new InvalidMessageBodyException())

        def jsonBuilder = Json.createObjectBuilder()
        jsonBuilder.add("password", PASSWORD)
        def bodyJson = jsonBuilder.build().toString()

        def spec = given().contentType(ContentType.JSON).body(bodyJson)
        when:
        def response = spec.post("/users")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.BAD_REQUEST
        response.asString() == expectedResponseJson
    }

    def "UserController should return proper code and information when password is missing in the request"() {
        given:
        def expectedResponseJson = ExceptionToJsonConverter.toJson(new InvalidMessageBodyException())

        def jsonBuilder = Json.createObjectBuilder()
        jsonBuilder.add("email", EMAIL)
        def bodyJson = jsonBuilder.build().toString()

        def spec = given().contentType(ContentType.JSON).body(bodyJson)
        when:
        def response = spec.post("/users")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.BAD_REQUEST
        response.asString() == expectedResponseJson
    }

    def "UserController should return proper code when there is no body in the request"() {
        def spec = given().contentType(ContentType.JSON)
        def expectedResponse = ExceptionToJsonConverter.toJson(new InvalidMessageBodyException());
        when:
        def response = spec.post("/users")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.BAD_REQUEST
        response.asString() == expectedResponse
    }

    def "UserController should return proper response when email is invalid"() {
        given:
        def incorrectEmail = "@gmail"
        def expectedResponseJson = ExceptionToJsonConverter.toJson(new InvalidEmailException(incorrectEmail))

        def jsonBuilder = Json.createObjectBuilder()
        jsonBuilder.add("email", incorrectEmail)
        jsonBuilder.add("password", PASSWORD)
        def bodyJson = jsonBuilder.build().toString()

        def spec = given().contentType(ContentType.JSON).body(bodyJson)
        when:
        def response = spec.post("/users")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.BAD_REQUEST
        response.asString() == expectedResponseJson
    }

    def "UserController should return BAD_REQUEST code and proper response when password is too weak"() {
        given:
        def expectedResponseJson = ExceptionToJsonConverter.toJson(new PasswordTooWeakException())

        def jsonBuilder = Json.createObjectBuilder()
        jsonBuilder.add("email", EMAIL)
        jsonBuilder.add("password", "123")
        def bodyJson = jsonBuilder.build().toString()

        def spec = given().contentType(ContentType.JSON).body(bodyJson)
        when:
        def response = spec.post("/users")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.BAD_REQUEST
        response.asString() == expectedResponseJson
    }

}
