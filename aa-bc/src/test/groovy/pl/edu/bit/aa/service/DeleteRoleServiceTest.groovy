package pl.edu.bit.aa.service

import pl.edu.bit.aa.db.PermissionRepository
import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.entity.Role
import spock.lang.Specification

class DeleteRoleServiceTest extends Specification {

    final static ROLE_NAME = "BATMAN"

    def rolesRepository = Mock(RolesRepository)

    PermissionRepository permissionRepository = Mock()

    UserService userService = Mock()

    RolesService service = new RolesService(userService, rolesRepository, permissionRepository)

    def 'Roles service should be able to successfully delete existing role'() {
        given:
        def roleToDelete = Mock(Role)
        rolesRepository.findRoleByName(ROLE_NAME) >> Optional.of(roleToDelete)
        when:
        service.delete(ROLE_NAME)
        then:
        1 * rolesRepository.delete({it == roleToDelete})
    }

    def 'Roles service should do nothing when role has been already deleted'() {
        given:
        rolesRepository.findRoleByName(ROLE_NAME) >> Optional.empty()
        when:
        service.delete(ROLE_NAME)
        then:
        0 * rolesRepository.delete(_)
    }
}
