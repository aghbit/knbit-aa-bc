package pl.edu.bit.aa.rest

import com.jayway.restassured.RestAssured
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import pl.edu.bit.aa.Application
import pl.edu.bit.aa.db.PermissionRepository
import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.db.TokenRepository
import pl.edu.bit.aa.db.UserRepository
import pl.edu.bit.aa.entity.Permission
import pl.edu.bit.aa.entity.Role
import pl.edu.bit.aa.entity.Token
import pl.edu.bit.aa.entity.User
import pl.edu.bit.aa.rest.util.RequestConstant
import pl.edu.bit.aa.spring.annotation.WebIntegrationTest
import spock.lang.Specification

import javax.json.Json

import static com.jayway.restassured.RestAssured.get
import static com.jayway.restassured.RestAssured.given

@ContextConfiguration(loader = SpringApplicationContextLoader.class,
        classes = [Application.class])
@WebIntegrationTest
class RolesControllerIT extends Specification {

    static final TOKEN = "1234"

    static final NAME_FIELD = "name"
    static final PERMISSIONS_FIELD = "permissions"
    static final AUTH_HEADER = RequestConstant.AUTH_HEADER

    static final ADMIN_EMAIL = "admin@email.com"
    static final USER_EMAIL = "user@email.com"
    static final PASSWORD = "hiperPassword"
    static final UUID ADMIN_UUID = UUID.randomUUID()
    static final UUID USER_UUID = UUID.randomUUID()
    static final TEST_ROLE = "testRole"

    static final ADMIN_ROLE = "ADMIN"

    Role adminRole
    Role roleToBeAssigned
    User admin
    User user
    Token token
    Permission adminPermission
    Permission userPermission

    @Value('${local.server.port}')
    int portNumber

    @Autowired
    RolesRepository rolesRepository

    @Autowired
    UserRepository userRepository

    @Autowired
    TokenRepository tokenRepository

    @Autowired
    PermissionRepository permissionRepository


    void setup() {
        RestAssured.port = portNumber
        flushDb()
        preparePermissions()

        adminRole = Role.create(ADMIN_ROLE, Collections.singleton(Permission.create("MANAGE_ROLES")))
        roleToBeAssigned = Role.create(TEST_ROLE, Collections.singleton(Permission.create("TEST")))
        rolesRepository.save(adminRole)
        rolesRepository.save(roleToBeAssigned)
        admin = User.create(ADMIN_UUID, ADMIN_EMAIL, PASSWORD, true, Collections.singleton(adminRole))
        user = User.create(USER_UUID, USER_EMAIL, PASSWORD, true, Collections.emptySet()) // user has no roles
        userRepository.save(admin)
        userRepository.save(user)
        token = Token.create(TOKEN, admin)
        tokenRepository.save(token)
    }

    def flushDb() {
        permissionRepository.deleteAll()
        rolesRepository.deleteAll()
        tokenRepository.deleteAll()
        userRepository.deleteAll()
    }

    private def preparePermissions() {
        adminPermission = Permission.create("MANAGE_ROLES")
        userPermission = Permission.create("TEST")
        permissionRepository.save(adminPermission)
        permissionRepository.save(userPermission)
    }

    void cleanup() {
        flushDb()
    }

    def "RolesController should return proper body after successful getting list of roles request"() {
        given:
        // ADMIN role and TEST role from setup()
        def spec = given().header(AUTH_HEADER, TOKEN)

        def expectedResponse = Json.createArrayBuilder()
                .add(Json.createObjectBuilder().add(NAME_FIELD, ADMIN_ROLE)
                .add(PERMISSIONS_FIELD, Json.createArrayBuilder().add(Permission.create("MANAGE_ROLES").getName())))
                .add(Json.createObjectBuilder().add(NAME_FIELD, TEST_ROLE)
                .add(PERMISSIONS_FIELD, Json.createArrayBuilder().add(Permission.create("TEST").getName())))
                .build().toString()
        when:
        def response = spec.get("/roles")
        then:
        HttpStatus.OK == HttpStatus.valueOf(response.statusCode())
        expectedResponse == response.getBody().asString()

    }

    def "RolesController should return proper info when user is not authenticated"() {
        given:
        tokenRepository.deleteAll()
        when:
        def response = get('/roles')
        then:
        HttpStatus.UNAUTHORIZED == HttpStatus.valueOf(response.statusCode())
    }

    def "RolesController should return proper info when user is not authorized"() {
        given:
        userRepository.deleteAll()
        admin = User.create(ADMIN_UUID, ADMIN_EMAIL, PASSWORD, true, Collections.emptySet())
        userRepository.save(admin)
        when:
        def response = get('/roles')
        then:
        HttpStatus.UNAUTHORIZED == HttpStatus.valueOf(response.statusCode())
    }

}
