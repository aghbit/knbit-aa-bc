package pl.edu.bit.aa.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.edu.bit.aa.entity.Token;
import pl.edu.bit.aa.rest.exception.UnauthorizedException;
import pl.edu.bit.aa.rest.exception.UserInactiveException;
import pl.edu.bit.aa.rest.json.*;
import pl.edu.bit.aa.rest.util.Authenticated;
import pl.edu.bit.aa.rest.util.RequestConstant;
import pl.edu.bit.aa.service.TokenService;
import pl.edu.bit.aa.service.exception.InvalidCredentialsException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Optional;

@RestController
class TokenController {

    @Autowired
    private TokenService tokenService;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public UserIdJson authenticate(@Valid @RequestBody TokenJson tokenJson, HttpServletResponse response) throws UnauthorizedException, UserInactiveException {
        String stringToken = tokenJson.getToken();
        Optional<Token> token = tokenService.findTokenAndUpdate(stringToken);
        if (!token.isPresent()) {
            throw new UnauthorizedException();
        } else if (!token.get().getUser().isActive()) {
            throw new UserInactiveException();
        } else {
            return new UserIdJson(token.get().getUser().getId().toString());
        }
    }

    @RequestMapping(value = "/request-token", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public TokenJson requestToken(@Valid @RequestBody TokenRequestJson tokenRequestJson) throws InvalidCredentialsException, UserInactiveException {
        Token token = tokenService.provideTokenByUserId(tokenRequestJson.getUserId(), tokenRequestJson.getPassword());
        return new TokenJson(token.getToken());
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public TokenWithUserIdJson login(@Valid @RequestBody SimpleCredentialsJson credentials) throws InvalidCredentialsException, UserInactiveException {
        Token token = tokenService.provideTokenByEmail(credentials.getEmail(), credentials.getPassword());
        return new TokenWithUserIdJson(token.getToken(), token.getUser().getId().toString());
    }

    /**
     * This controller has token validated by handler interceptor because of @{@link Authenticated} annotation.
     * If token is valid, this controller performs token removal. Otherwise HTTP 401 status is returned.
     */
    @RequestMapping(value = "/logout", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Authenticated
    public void logout(@RequestHeader(RequestConstant.AUTH_HEADER) String stringToken){
        tokenService.removeToken(stringToken);
    }
}
