package pl.edu.bit.aa.service;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.bit.aa.db.PermissionRepository;
import pl.edu.bit.aa.entity.Permission;
import pl.edu.bit.aa.service.exception.AlreadyExistingPermissionException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PermissionService {

    private final PermissionRepository permissionRepository;

    @Autowired
    PermissionService(@NonNull PermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }

    public List<Permission> getAllPermissions() {
        return permissionRepository.findAll().stream()
                .filter(permission -> !permission.getName().equals("ADMIN"))
                .collect(Collectors.toList());
    }

    public void createPermission(@NonNull String name) throws AlreadyExistingPermissionException {
        Optional<Permission> permission = permissionRepository.findPermissionByName(name);
        if (permission.isPresent()) {
            throw new AlreadyExistingPermissionException();
        }
        permissionRepository.save(Permission.create(name.toUpperCase()));
    }
}
