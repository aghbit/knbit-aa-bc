package pl.edu.bit.aa.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class TokenWithUserIdJson {
    private final String token;
    private final String userId;

    @JsonCreator
    public TokenWithUserIdJson(
            @JsonProperty("token") String token,
            @JsonProperty("userId") String userId){
        this.token = token;
        this.userId = userId;
    }

}
