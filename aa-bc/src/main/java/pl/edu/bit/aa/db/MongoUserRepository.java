package pl.edu.bit.aa.db;

import lombok.NonNull;
import org.mongodb.morphia.Datastore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.edu.bit.aa.entity.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
class MongoUserRepository implements UserRepository {

    @Autowired
    private Datastore ds;

    @Override
    public Optional<User> findById(@NonNull String userId) {
        UUID userUUID;
        try {
            userUUID = UUID.fromString(userId);
        } catch (IllegalArgumentException e){
            return Optional.empty();
        }
        return Optional.ofNullable(ds.find(User.class).field("id").equal(userUUID).get());
    }

    @Override
    public Optional<User> findByEmail(@NonNull String email) {
        return Optional.ofNullable(ds.find(User.class).field("email").equal(email).get());
    }

    @Override
    public List<User> findAll() {
        return ds.find(User.class).asList();
    }

    @Override
    public boolean containsId(@NonNull String id) {
        return findById(id).isPresent();
    }


    @Override
    public void save(@NonNull User user) {
        ds.save(user);
    }

    @Override
    public void deleteAll() {
        ds.delete(ds.find(User.class));
    }
}
