package pl.edu.bit.aa.db;

import pl.edu.bit.aa.entity.Permission;

import java.util.List;
import java.util.Optional;

public interface PermissionRepository {

    Optional<Permission> findPermissionByName(String permissionName);

    List<Permission> findAll();

    void save(Permission permission);

    long countAll();

    void deleteAll();
}
