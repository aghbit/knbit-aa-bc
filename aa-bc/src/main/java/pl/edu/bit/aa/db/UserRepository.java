package pl.edu.bit.aa.db;

import pl.edu.bit.aa.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    Optional<User> findById(String userId);

    Optional<User> findByEmail(String email);

    List<User> findAll();

    boolean containsId(String id);

    void save(User user);

    void deleteAll();
}

