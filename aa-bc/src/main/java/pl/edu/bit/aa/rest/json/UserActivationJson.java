package pl.edu.bit.aa.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class UserActivationJson {
    private final boolean active;

    @JsonCreator
    public UserActivationJson(@JsonProperty(value = "active") boolean active) {
        this.active = active;
    }
}
