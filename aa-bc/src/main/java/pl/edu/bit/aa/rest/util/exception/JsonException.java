package pl.edu.bit.aa.rest.util.exception;

import org.springframework.http.HttpStatus;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Class annotation that marks class as exception converted to json.
 *
 * Exceptions annotated with {@code JsonException} will be converted to json including only fields with getters.
 * <p>
 * This class intended for Java classes usage only!
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonException {

    /**
     * The status code to use for the response.
     *
     * @see javax.servlet.http.HttpServletResponse#setStatus(int)
     */
    HttpStatus value();
}