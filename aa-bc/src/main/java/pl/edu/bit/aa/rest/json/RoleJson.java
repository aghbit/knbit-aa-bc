package pl.edu.bit.aa.rest.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.Set;

@Value
public class RoleJson {
    private final String name;
    private final Set<String> permissions;

    public RoleJson(@JsonProperty("name") String name, @JsonProperty("permissions") Set<String> permissions) {
        this.name = name;
        this.permissions = permissions;
    }
}
