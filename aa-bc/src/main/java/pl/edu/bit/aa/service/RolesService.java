package pl.edu.bit.aa.service;

import com.google.common.collect.Sets;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.bit.aa.db.PermissionRepository;
import pl.edu.bit.aa.db.RolesRepository;
import pl.edu.bit.aa.entity.Permission;
import pl.edu.bit.aa.entity.Role;
import pl.edu.bit.aa.rest.exception.CannotRemoveAdminRoleException;
import pl.edu.bit.aa.service.exception.AlreadyExistingRoleException;
import pl.edu.bit.aa.service.exception.NoSuchPermissionException;
import pl.edu.bit.aa.service.exception.RoleNotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class RolesService {

    private final UserService userService;

    private final RolesRepository rolesRepository;

    private final PermissionRepository permissionRepository;

    @Autowired
    RolesService(@NonNull UserService userService, @NonNull RolesRepository rolesRepository, PermissionRepository
            permissionRepository) {
        this.userService = userService;
        this.rolesRepository = rolesRepository;
        this.permissionRepository = permissionRepository;
    }

    public List<Role> getAllRoles() {
        return rolesRepository.findAll();
    }

    public void createRole(@NonNull String name, @NonNull Set<String> permissionNames)
            throws AlreadyExistingRoleException, NoSuchPermissionException {
        String roleName = name.toUpperCase(); // having 'ADMIN' and 'admin' as different roles would be silly.
        Optional<Role> role = rolesRepository.findRoleByName(roleName);
        if (role.isPresent()) {
            throw new AlreadyExistingRoleException();
        }

        Set<Permission> permissions = convertPermissions(permissionNames);

        Role newRole = Role.create(roleName, permissions);
        rolesRepository.save(newRole);
    }

    /**
     * Tries to convert from name to {@link Permission} instance.
     *
     * @throws NoSuchPermissionException if some permission does not exist.
     */
    private Set<Permission> convertPermissions(@NonNull Set<String> permissionNames) throws NoSuchPermissionException {
        Set<Permission> permissions = Sets.newHashSet();

        for (String name : permissionNames) {
            Optional<Permission> permission = permissionRepository.findPermissionByName(name);
            if (permission.isPresent()) {
                permissions.add(permission.get());
            } else {
                throw new NoSuchPermissionException(name);
            }
        }

        return permissions;
    }


    public void delete(@NonNull String name) throws CannotRemoveAdminRoleException {
        String roleName = name.toUpperCase();
        if (roleName.equals("ADMIN")) {
            throw new CannotRemoveAdminRoleException();
        }
        Optional<Role> role = rolesRepository.findRoleByName(roleName);

        if (role.isPresent()) {
            userService.deleteRoleFromAllUsers(role.get());
            rolesRepository.delete(role.get());
        }
    }

    public void update(@NonNull String oldName, @NonNull String newName, @NonNull Set<String> newPermissionNames)
            throws AlreadyExistingRoleException, NoSuchPermissionException, RoleNotFoundException {
        String newRoleName = newName.toUpperCase();
        String oldRoleName = oldName.toUpperCase();

        Optional<Role> roleToChange = rolesRepository.findRoleByName(oldRoleName);

        if (!roleToChange.isPresent()) {
            throw new RoleNotFoundException();
        }

        if (!oldRoleName.equals(newRoleName) && rolesRepository.findRoleByName(newRoleName).isPresent()) {
            throw new AlreadyExistingRoleException();
        }

        if (!oldRoleName.equals(newRoleName)) {
            rolesRepository.delete(roleToChange.get());
        }

        Set<Permission> newPermissions = convertPermissions(newPermissionNames);

        rolesRepository.save(Role.create(newRoleName, newPermissions));
    }

    public Role get(@NonNull String name) throws RoleNotFoundException {
        Optional<Role> role = rolesRepository.findRoleByName(name.toUpperCase());
        if (role.isPresent()) {
            return role.get();
        } else {
            throw new RoleNotFoundException();
        }
    }
}
