package pl.edu.bit.aa.db;

import pl.edu.bit.aa.entity.Token;

import java.util.Optional;

public interface TokenRepository {

    Optional<Token> findTokenAndUpdate(String token);

    void save(Token token);

    /**
     * This function operates in an idempotent manner. Consecutive calls will not cause exceptional behaviour.
     * That is, even if the token does not exist, no exception will be thrown and no message will be returned.
     * @param stringToken token to remove
     */
    void delete(String stringToken);

    void deleteAll();
}
