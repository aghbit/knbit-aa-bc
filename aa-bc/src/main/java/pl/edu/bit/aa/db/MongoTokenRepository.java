package pl.edu.bit.aa.db;

import lombok.NonNull;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.edu.bit.aa.entity.Token;

import java.util.Date;
import java.util.Optional;

@Repository
class MongoTokenRepository implements TokenRepository {

    @Autowired
    private Datastore ds;

    @Override
    public Optional<Token> findTokenAndUpdate(@NonNull String stringToken) {
        Query<Token> query = ds.createQuery(Token.class).field("token").equal(stringToken);
        UpdateOperations<Token> update = ds.createUpdateOperations(Token.class).set("lastUsed", new Date());
        return Optional.ofNullable(ds.findAndModify(query, update));
    }

    @Override
    public void save(Token token) {
        ds.save(token);
    }

    @Override
    public void delete(String stringToken) {
        ds.delete(ds.find(Token.class).field("token").equal(stringToken));
    }

    @Override
    public void deleteAll() {
        ds.delete(ds.find(Token.class));
    }
}
