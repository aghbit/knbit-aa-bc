package pl.edu.bit.aa.rest.util;

public interface RequestConstant {
    String AUTH_HEADER = "knbit-aa-auth";
    String USER_ID_ATTR = "knbit-aa-userid";
}
