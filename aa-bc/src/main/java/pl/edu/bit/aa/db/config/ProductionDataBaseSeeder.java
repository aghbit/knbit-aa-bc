package pl.edu.bit.aa.db.config;

import com.google.common.collect.Sets;
import lombok.Data;
import org.jasypt.util.password.PasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import pl.edu.bit.aa.config.ApplicationProfile;
import pl.edu.bit.aa.db.PermissionRepository;
import pl.edu.bit.aa.db.RolesRepository;
import pl.edu.bit.aa.db.UserRepository;
import pl.edu.bit.aa.entity.Permission;
import pl.edu.bit.aa.entity.Role;
import pl.edu.bit.aa.entity.User;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Data
@Component
@PropertySource("classpath:production-database-seeder.properties")
@Profile(ApplicationProfile.PRODUCTION)
public class ProductionDataBaseSeeder {

    @Value("${db.admin.email}")
    private String adminEmail;
    @Value("${db.admin.password}")
    private String adminPassword;

    @Value("${db.microservice.email}")
    private String microserviceEmail;
    @Value("${db.microservice.password}")
    private String microservicePassword;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RolesRepository rolesRepository;
    @Autowired
    private PasswordEncryptor encryptor;
    @Autowired
    private PermissionsDataBaseSeeder permissionsDataBaseSeeder;
    @Autowired
    private PermissionRepository permissionRepository;

    @PostConstruct
    public void provide() {

        if (permissionRepository.countAll() == 0) { // if no persisted permissions yet
            permissionsDataBaseSeeder.insertDefaultPermissions();
            Set<Permission> adminPermissions = permissionsDataBaseSeeder.getAllPermissions();

            rolesRepository.deleteAll();
            Role adminRole = Role.create("ADMIN", adminPermissions);
            rolesRepository.save(adminRole);

            Set<Permission> sectionManagerPermissions = Sets.newHashSet(
                    Permission.create("MANAGE_SECTION"),
                    Permission.create("SEND_MAIL"));
            rolesRepository.save(Role.create("SECTION_MANAGER", sectionManagerPermissions));

            Optional<User> existingAdmin = userRepository.findByEmail(adminEmail);
            UUID adminId = existingAdmin.isPresent() ? existingAdmin.get().getId() : UUID.randomUUID();

            User admin = User.create(
                    adminId,
                    adminEmail,
                    encryptor.encryptPassword(adminPassword),
                    true,
                    Collections.singleton(adminRole));

            userRepository.save(admin);
        }

        if (!userRepository.findByEmail(microserviceEmail).isPresent()) { // create microservice account
            User microserviceAccount = User.create(
                    UUID.randomUUID(),
                    microserviceEmail,
                    encryptor.encryptPassword(microservicePassword),
                    true,
                    Collections.singleton(rolesRepository.findRoleByName("ADMIN").get()));
            userRepository.save(microserviceAccount);
        }
    }

}
