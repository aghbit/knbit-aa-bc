package pl.edu.bit.aa.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class PasswordChangeJson {
    private final String oldPassword;
    private final String newPassword;

    @JsonCreator
    public PasswordChangeJson(@JsonProperty("oldPassword") String oldPassword, @JsonProperty("newPassword") String newPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }
}
