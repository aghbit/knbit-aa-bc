package pl.edu.bit.aa.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.edu.bit.aa.rest.json.PermissionJson;
import pl.edu.bit.aa.rest.util.Authorized;
import pl.edu.bit.aa.service.PermissionService;
import pl.edu.bit.aa.service.exception.AlreadyExistingPermissionException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class PermissionController {

    @Autowired
    PermissionService permissionService;

    @RequestMapping(
            value = "/permissions",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Authorized(permissionName = "MANAGE_ROLES")
    public List<PermissionJson> getAllPermissions() {
        return permissionService.getAllPermissions().stream()
                .map((permission) -> new PermissionJson(permission.getName()))
                .collect(Collectors.toList());
    }

    @RequestMapping(
            value = "/permissions",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Authorized(permissionName = "MANAGE_ROLES")
    public void createPermission(@Valid @RequestBody PermissionJson permission) throws AlreadyExistingPermissionException {
        permissionService.createPermission(permission.getName());
    }
}
