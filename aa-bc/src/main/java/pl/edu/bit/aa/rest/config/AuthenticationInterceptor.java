package pl.edu.bit.aa.rest.config;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import pl.edu.bit.aa.entity.Token;
import pl.edu.bit.aa.rest.util.Authenticated;
import pl.edu.bit.aa.rest.util.RequestConstant;
import pl.edu.bit.aa.service.AuthenticationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Intercepts request for authentication purpose.
 * <p>
 * If controller method is annotated with {@link Authenticated} annotation,
 * then {@code AuthenticationInterceptor} will extract auth token from request header and validate it.
 * Interceptor will also check if user is active.
 * <p>
 * If auth token is valid, interceptor will add UserId as attribute to the request
 * and pass the request to the actual request handler method.
 */
@Component
class AuthenticationInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (shouldAuthenticate(handler)) {
            Optional<Token> token = provideTokenFor(request);
            if (token.isPresent()) {
                request.setAttribute(RequestConstant.USER_ID_ATTR, token.get().getUser().getId().toString());
                return true;
            } else {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return false;
            }
        }
        return true;
    }

    private boolean shouldAuthenticate(@NonNull Object handler) {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Optional<Authenticated> authenticatedAnnotation = Optional.ofNullable(handlerMethod.getMethodAnnotation(Authenticated.class));
            return authenticatedAnnotation.isPresent();
        }
        return false;
    }

    /**
     * Extracts token from request and tries to find it in DB.
     * @param request httpServletRequest
     * @return token if found or Optional.empty() otherwise
     */
    private Optional<Token> provideTokenFor(@NonNull HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader(RequestConstant.AUTH_HEADER))
                .flatMap(authenticationService::authenticateUser);
    }
}
