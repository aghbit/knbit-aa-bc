package pl.edu.bit.aa.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import pl.edu.bit.aa.rest.json.valid.BasicValidator;

/**
 * Binds {@link BasicValidator} to all Controllers, so {@link javax.validation.Valid} annotation can be used
 * without additional configuration in every Controller.
 */
@ControllerAdvice
public class ValidatorBinder {

    @Autowired
    private BasicValidator basicValidator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(basicValidator);
    }

}
