package pl.edu.bit.aa.service;

import lombok.Data;
import lombok.NonNull;
import org.jasypt.digest.StringDigester;
import org.jasypt.util.password.PasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.bit.aa.db.TokenRepository;
import pl.edu.bit.aa.db.UserRepository;
import pl.edu.bit.aa.entity.Token;
import pl.edu.bit.aa.entity.User;
import pl.edu.bit.aa.rest.exception.UserInactiveException;
import pl.edu.bit.aa.service.exception.InvalidCredentialsException;

import java.util.Optional;
import java.util.UUID;


@Service
@Data
public class TokenService {

    private final TokenRepository tokenRepository;

    private final UserRepository userRepository;

    private final PasswordEncryptor encryptor;

    private final StringDigester digester;

    @Autowired
    TokenService(@NonNull TokenRepository tokenRepository,
                 @NonNull UserRepository userRepository,
                 @NonNull PasswordEncryptor encryptor,
                 @NonNull StringDigester digester) {
        this.tokenRepository = tokenRepository;
        this.userRepository = userRepository;
        this.encryptor = encryptor;
        this.digester = digester;
    }

    public Optional<Token> findTokenAndUpdate(@NonNull String stringToken) {
        return tokenRepository.findTokenAndUpdate(stringToken);
    }

    public boolean isTokenValid(@NonNull String stringToken) {
        return findTokenAndUpdate(stringToken).isPresent();
    }

    public void removeToken(@NonNull String stringToken) {
        tokenRepository.delete(stringToken);
    }

    public Token provideTokenByEmail(@NonNull String email, @NonNull String password) throws InvalidCredentialsException, UserInactiveException {
        Optional<User> user = userRepository.findByEmail(email);
        return doProvideToken(user, password);
    }

    public Token provideTokenByUserId(@NonNull String userId, @NonNull String password) throws InvalidCredentialsException, UserInactiveException {
        Optional<User> user = userRepository.findById(userId);
        return doProvideToken(user, password);
    }

    private Token doProvideToken(Optional<User> user, String password) throws InvalidCredentialsException, UserInactiveException {
        if (!user.isPresent()) {
            throw new InvalidCredentialsException();
        }
        if (!user.get().isActive()) {
            throw new UserInactiveException();
        }
        if (!encryptor.checkPassword(password, user.get().getPassword())) {
            throw new InvalidCredentialsException();
        }
        String encryptedToken;
        do {
            UUID uuid = UUID.randomUUID();
            encryptedToken = digester.digest(uuid.toString());
        } while (tokenRepository.findTokenAndUpdate(encryptedToken).isPresent());
        Token token = Token.create(encryptedToken, user.get());
        tokenRepository.save(token);
        return token;

    }

}
