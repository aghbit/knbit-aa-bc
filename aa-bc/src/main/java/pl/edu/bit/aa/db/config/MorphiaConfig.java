package pl.edu.bit.aa.db.config;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.ServerAddress;
import lombok.SneakyThrows;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MorphiaConfig {
    @Value("${bit.aa.mongo.host}")
    private String hostname;

    @Value("${bit.aa.mongo.port}")
    private int port;

    @Value("${bit.aa.mongo.db}")
    private String dbName;

    @Bean
    @SneakyThrows
    public Datastore provideDatastore() {
        String password = System.getenv("MONGO_PASSWORD");
        if (password == null) {
            throw new IllegalStateException("No mongo password in environment variables. Please set MONGO_PASSWORD variable");
        }
        Datastore ds = new Morphia()
                .mapPackage("pl.edu.bit.aa.entity")
                .createDatastore(new MongoClient(new MongoClientURI("mongodb://knbitaa:" + password + "@ds055885.mongolab.com:55885/" + dbName)), dbName);

        ds.ensureIndexes(); //creates indexes from @Index annotations in your entities
        ds.ensureCaps(); //creates capped collections from @Entity

        return ds;
    }
}
