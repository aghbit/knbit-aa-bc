package pl.edu.bit.aa.rest.config;

import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import pl.edu.bit.aa.rest.util.Authenticated;
import pl.edu.bit.aa.rest.util.RequestConstant;
import pl.edu.bit.aa.rest.util.UserId;

import java.util.Optional;

/**
 * Extracts userId attribute from request and resolves it as an argument of handler method.
 * <p>
 * Resolver supports only methods which are annotated with {@link Authenticated}
 * (to make sure, that {@link AuthenticationInterceptor} has done its job) and resolves
 * only {@link String} argument annotated with {@link UserId}.
 * <p>
 * This resolver strongly relies on behaviour of {@link AuthenticationInterceptor}
 * - after successful authentication, the interceptor has to properly set request attribute with userId.
 */
@Component
class AuthenticatedUserArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return Optional.ofNullable(parameter.getMethodAnnotation(Authenticated.class)).isPresent()
                && parameter.hasParameterAnnotation(UserId.class)
                && parameter.getParameterType() == String.class;
    }

    @Override
    public Object resolveArgument(
            MethodParameter parameter,
            ModelAndViewContainer mavContainer,
            NativeWebRequest webRequest,
            WebDataBinderFactory binderFactory) throws Exception {
        return webRequest.getAttribute(RequestConstant.USER_ID_ATTR, RequestAttributes.SCOPE_REQUEST);
    }
}
