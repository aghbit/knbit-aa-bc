package pl.edu.bit.aa.db.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.edu.bit.aa.db.PermissionRepository;
import pl.edu.bit.aa.entity.Permission;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@Component
public class PermissionsDataBaseSeeder {

    private final PermissionRepository permissionRepository;

    private final Set<Permission> allPermissions;

    @Autowired
    public PermissionsDataBaseSeeder(PermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
        List<String> permissionNames = Arrays.asList(
                "ADMIN", // fixme delete admin permission as soon as all BCs stop using it
                "MANAGE_ROLES",
                "MANAGE_USERS",
                "MANAGE_TIMELINE_PERIODS",
                "MANAGE_ALL_SECTIONS",
                "MANAGE_SECTION",
                "SEND_MAIL",
                "PROJECTS_ENROLL",
                "EVENTS_MANAGEMENT");
        allPermissions = permissionNames.stream()
                .map(Permission::create)
                .collect(Collectors.toSet());
    }

    public void insertDefaultPermissions() {
        allPermissions.forEach(permissionRepository::save);
    }

    public Set<Permission> getAllPermissions() {
        return Collections.unmodifiableSet(allPermissions);
    }
}



