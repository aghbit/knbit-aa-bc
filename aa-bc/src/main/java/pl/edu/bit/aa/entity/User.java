package pl.edu.bit.aa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;

import java.util.Collections;
import java.util.Set;
import java.util.UUID;

@Data
@EqualsAndHashCode(of = "id")
@AllArgsConstructor(staticName = "create")
@Entity
public class User {
    @Id
    private final UUID id;

    @Indexed(unique = true, name = "email")
    private final String email;
    private final String password;
    private final boolean active;
    @Reference
    private final Set<Role> roles;

    // MongoDB purpose only
    private User() {
        this.id = null;
        this.email = null;
        this.password = null;
        this.active = false;
        this.roles = Collections.emptySet();
    }

    public static User create(@NonNull String email, @NonNull String password) {
        return new User(UUID.randomUUID(), email, password, false, Collections.emptySet());
    }

    public Set<Role> getRoles() {
        return Collections.unmodifiableSet(roles);
    }
}
