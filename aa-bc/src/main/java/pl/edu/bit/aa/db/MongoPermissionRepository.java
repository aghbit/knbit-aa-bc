package pl.edu.bit.aa.db;

import lombok.NonNull;
import org.mongodb.morphia.Datastore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.edu.bit.aa.entity.Permission;

import java.util.List;
import java.util.Optional;

@Repository
class MongoPermissionRepository implements PermissionRepository {

    @Autowired
    private Datastore ds;

    @Override
    public Optional<Permission> findPermissionByName(@NonNull String permissionName) {
        return Optional.ofNullable(ds.find(Permission.class).field("name").equal(permissionName).get());
    }

    @Override
    public List<Permission> findAll() {
        return ds.find(Permission.class).asList();
    }

    @Override
    public void save(@NonNull Permission permission) {
        ds.save(permission);
    }

    @Override
    public long countAll() {
        return ds.getCount(ds.find(Permission.class));
    }

    @Override
    public void deleteAll() {
        ds.delete(ds.find(Permission.class));
    }
}
