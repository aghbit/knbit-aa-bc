package pl.edu.bit.aa.service;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.bit.aa.entity.Token;

import java.util.Optional;

/**
 * @author Wojciech Milewski
 */
@Service
public class AuthenticationService {

    private final TokenService tokenService;

    @Autowired
    public AuthenticationService(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    /**
     * Returns Optional containing token if passed token is valid and
     * user is active, Optional.empty() otherwise.
     */
    public Optional<Token> authenticateUser(@NonNull String token) {
        Optional<Token> userToken = tokenService.findTokenAndUpdate(token);

        return userToken.filter(t -> t.getUser().isActive());
    }
}
