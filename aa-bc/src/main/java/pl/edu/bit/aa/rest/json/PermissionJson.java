package pl.edu.bit.aa.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
public class PermissionJson {
    private final String name;

    @JsonCreator
    public PermissionJson(@JsonProperty("name") String name) {
        this.name = name;
    }
}
