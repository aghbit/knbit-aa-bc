package pl.edu.bit.aa.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.mongodb.morphia.annotations.Id;

@Data
@EqualsAndHashCode(of = "name")
@RequiredArgsConstructor(staticName = "create")
public class Permission {
    @Id
    private final String name;

    /**
     * MongoDB purpose only. Do not use!
     */
    private Permission() {
        name = "";
    }
}
