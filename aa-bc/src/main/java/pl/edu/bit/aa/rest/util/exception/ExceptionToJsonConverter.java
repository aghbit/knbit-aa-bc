package pl.edu.bit.aa.rest.util.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.reflect.TypeToken;

import java.beans.Introspector;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public interface ExceptionToJsonConverter {

    static String toJson(Object o) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        Map<String, Object> fields = Arrays.asList(o.getClass().getDeclaredMethods()).stream()
                .filter(method -> TypeToken.of(o.getClass()).method(method).isPublic())
                .filter(method -> method.getName().startsWith("get"))
                .filter(method -> method.getParameterCount() == 0)
                .filter(method -> {
                    try {
                        //get only methods which can be successfully invoked
                        method.invoke(o);
                        return true;
                    } catch (ReflectiveOperationException e) {
                        return false;
                    }
                })
                .collect(Collectors.toMap(
                        method -> Introspector.decapitalize(method.getName().substring(3)),
                        method -> {
                            try {
                                return method.invoke(o);
                            } catch (ReflectiveOperationException e) {
                                //will not happen due to filtering before
                                return null;
                            }
                        }));
        return mapper.writeValueAsString(fields);
    }

}
