package pl.edu.bit.aa.service;

import lombok.NonNull;
import org.springframework.stereotype.Component;

import static org.apache.commons.lang3.StringUtils.length;
import static org.apache.commons.lang3.StringUtils.strip;

@Component
class PasswordStrengthPolicy {

    public boolean isPasswordStrong(@NonNull String password) {
        if (length(strip(password)) < 6) {
            return false;
        }
        return true;
    }
}
