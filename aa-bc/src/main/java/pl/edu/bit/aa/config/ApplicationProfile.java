package pl.edu.bit.aa.config;

public interface ApplicationProfile {
    String DEVELOPMENT = "development";
    String PRODUCTION = "production";
    String TEST = "test";
}
