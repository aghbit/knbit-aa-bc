package pl.edu.bit.aa.db;

import lombok.NonNull;
import org.mongodb.morphia.Datastore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.edu.bit.aa.entity.Role;

import java.util.List;
import java.util.Optional;

@Repository
class MongoRolesRepository implements RolesRepository {

    @Autowired
    private Datastore ds;

    @Override
    public Optional<Role> findRoleByName(@NonNull String roleName) {
        return Optional.ofNullable(ds.find(Role.class).field("name").equal(roleName).get());
    }

    @Override
    public List<Role> findAll() {
        return ds.find(Role.class).asList();
    }

    @Override
    public long countAll() {
        return ds.getCount(ds.find(Role.class));
    }

    @Override
    public void save(@NonNull Role role) {
        ds.save(role);
    }

    @Override
    public void delete(@NonNull Role role) {
        ds.delete(role);
    }

    @Override
    public void deleteAll() {
        ds.delete(ds.find(Role.class));
    }
}
