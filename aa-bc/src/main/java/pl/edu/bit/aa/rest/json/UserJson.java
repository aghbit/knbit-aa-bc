package pl.edu.bit.aa.rest.json;

import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.List;

@Value
@RequiredArgsConstructor(staticName = "create")
public class UserJson {
    private final String id;
    private final String email;
    private final List<String> roles;
    private final boolean active;

}
