package pl.edu.bit.aa.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.edu.bit.aa.rest.exception.ForbiddenException;
import pl.edu.bit.aa.rest.json.AuthorizationJson;
import pl.edu.bit.aa.rest.json.UserIdJson;
import pl.edu.bit.aa.rest.util.Authenticated;
import pl.edu.bit.aa.rest.util.UserId;
import pl.edu.bit.aa.service.AuthorizationService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
class AuthorizationController {

    @Autowired
    private AuthorizationService authorizationService;

    @RequestMapping(
            value = "/authorize",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Authenticated
    @ResponseStatus(HttpStatus.OK)
    public UserIdJson authorize(@UserId String userId,
                          @Valid @RequestBody AuthorizationJson body,
                          HttpServletResponse response) throws ForbiddenException {
        if (!authorizationService.userHasPermission(userId, body.getPermission())) {
            throw new ForbiddenException();
        } else {
            return new UserIdJson(userId);
        }
    }
}
