package pl.edu.bit.aa.db;

import pl.edu.bit.aa.entity.Role;

import java.util.List;
import java.util.Optional;

public interface RolesRepository {

    Optional<Role> findRoleByName(String roleName);

    List<Role> findAll();

    void save(Role role);

    void delete(Role role);

    void deleteAll();

    long countAll();
}
