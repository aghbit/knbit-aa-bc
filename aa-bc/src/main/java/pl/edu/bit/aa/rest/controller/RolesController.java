package pl.edu.bit.aa.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.edu.bit.aa.entity.Permission;
import pl.edu.bit.aa.entity.Role;
import pl.edu.bit.aa.rest.exception.CannotRemoveAdminRoleException;
import pl.edu.bit.aa.rest.json.RoleJson;
import pl.edu.bit.aa.rest.util.Authorized;
import pl.edu.bit.aa.service.AuthorizationService;
import pl.edu.bit.aa.service.RolesService;
import pl.edu.bit.aa.service.exception.AlreadyExistingRoleException;
import pl.edu.bit.aa.service.exception.NoSuchPermissionException;
import pl.edu.bit.aa.service.exception.RoleNotFoundException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
class RolesController {

    @Autowired
    AuthorizationService authorizationService;

    @Autowired
    private RolesService rolesService;

    @RequestMapping(
            value = "/roles",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Authorized(permissionName = "MANAGE_ROLES")
    public List<RoleJson> getAllRoles() {
        return rolesService.getAllRoles().stream()
                .map((role) -> new RoleJson(role.getName(), role.getPermissions()
                        .stream().map(Permission::getName)
                        .collect(Collectors.toSet())))
                .collect(Collectors.toList());
    }

    @RequestMapping(
            value = "/roles/{name}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Authorized(permissionName = "MANAGE_ROLES")
    public RoleJson getRole(@PathVariable String name) throws RoleNotFoundException {
        Role role = rolesService.get(name);
        return new RoleJson(role.getName(), role.getPermissions().stream()
                .map(Permission::getName)
                .collect(Collectors.toSet()));
    }

    @RequestMapping(
            value = "/roles",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Authorized(permissionName = "MANAGE_ROLES")
    public void createRole(@Valid @RequestBody RoleJson json)
            throws AlreadyExistingRoleException, NoSuchPermissionException {
        rolesService.createRole(json.getName(), json.getPermissions());
    }

    @RequestMapping(
            value = "/roles/{name}",
            method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Authorized(permissionName = "MANAGE_ROLES")
    public void deleteRole(@PathVariable String name) throws CannotRemoveAdminRoleException {
        rolesService.delete(name);
    }


    @RequestMapping(
            value = "/roles/{name}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Authorized(permissionName = "MANAGE_ROLES")
    public void updateRole(@PathVariable String name, @Valid @RequestBody RoleJson json)
            throws AlreadyExistingRoleException, NoSuchPermissionException, RoleNotFoundException {
        rolesService.update(name, json.getName(), json.getPermissions());
    }
}
